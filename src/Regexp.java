
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Denis Leontev
 *
 * Реализация метода который ищет слова начинающиеся с при, пре
 */
public class Regexp {
    public static void main(String[] args) throws IOException {
        String s;
        Pattern pattern = Pattern.compile("\\b(пре|при|При|Пре) [а-яё]+\\b");
        Matcher matcher = pattern.matcher("");

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src/text"))) {
            while ((s = bufferedReader.readLine()) != null) {
                matcher.reset(s);
                while (matcher.find()) {
                    System.out.println(matcher.group());
                }
            }
        }
    }
}

